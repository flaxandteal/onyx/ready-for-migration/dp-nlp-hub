package payloads

type Hub struct {
	Scrubber ScrubberSearchSchemaJson
	Category Category
	Berlin   BerlinSearchSchemaJson
}
